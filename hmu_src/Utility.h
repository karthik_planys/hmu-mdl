/*
 *	File: Utility.h
 *	Created: 11 Oct 2018, 01:17 AM
 *  Author: Vineet Upadhyay
 * 	Project: Mikros
 * 	© Planys Technologies Pvt. Ltd., Chennai
 *
 *  v1.00
 *
 * 	NOTES:
 *
 */

#ifndef UTILITY_H
#define UTILITY_H

#include "shared_memory.h"
//#include "Config.h"
//#include "peripherals/Thruster.h"
#include "SerialConnection.h"
#include "SerialEvent.h"

#include <boost/thread.hpp>
#include <boost/crc.hpp>
#include <boost/cstdint.hpp>
#include <deque>
#include <openssl/bio.h>
#include <openssl/evp.h>
#include <vector>

// Macros
#define TOSTRING(x) #x

// Main Board Variables
//extern int ls_hull, ls_cam_encl, eeh_amb_temp;

//extern SerialConnection SMCUConnection;
//extern SerialConnection MainBoardConnection;
extern SerialConnection HMUConnection;
extern SMemory s_memory;

//void recvFromMainBoard (const char*, size_t);
//void recvFromSMCU (const char*, size_t);
void recvFromHMU (const char*, size_t);
//void parseSMCUData ();
//void parseMBDData ();
void parseHMUData ();
void closeSerialHandlers ();
void delay (int);
void delayMicroseconds (int);
void sendEvent (SerialEvent *, SerialConnection *);
long long getCurrentTimeMillis ();
std::string base64_encode (const std::string &);
std::string base64_decode (const std::string &);
//float getCurrentHeading ();
//float getCurrentPitch ();
//float getCurrentRoll ();
//float getCurrentDepth ();
//float getCurrentAltitude ();
std::vector<string> split (const string&, const string&);
void autoThrusterCheck ();

#endif
