/*
 *	File: SerialEvent.cpp
 *	Created: 11 Oct 2018, 03:27 PM
 *  Author: Vineet Upadhyay
 * 	Project: Mikros
 * 	© Planys Technologies Pvt. Ltd., Chennai
 *
 *  v1.00
 *
 * 	NOTES:
 *
 */

#include "SerialEvent.h"

SerialEvent::SerialEvent()
{
	//printf("Serial Event");
	packet_size = 6;
	reset();
}

void SerialEvent::reset()
{
	identifier = 0x00;
	event = 0x00;
	value = 0x00;
	_value[0] = 0x00;
	_value[1] = 0x00;
	checksum = 0x00;
	_checksum[0] = 0x00;
	_checksum[1] = 0x00;
}

void SerialEvent::create(unsigned char _iden, unsigned char _evt, uint16_t _val)
//void SerialEvent::create(unsigned char _iden, unsigned char _evt, unsigned char value0, unsigned char value1)
//void SerialEvent::create(unsigned char _iden, unsigned char _evt, unsigned char value0, unsigned char value1)
{
	identifier = _iden;
	event = _evt;
	value = _val;

	// Split the high and low bytes of variable 'value'
	_value[0] = (value >> 8) & 0xFF; // High byte
	_value[1] = value & 0xFF;		 // Low byte

	//_value[0] = (char)value/10;
	//_value[1] = (char)value%10; 		 // Low byte

	cout<<"value 0 i.e. high byte is "<<(unsigned char)_value[0]<<endl;
	cout<<"value 1 i.e. low byte is  "<<(unsigned char)_value[1]<<endl;


	// Calculate checksum and split the high and low bytes
	unsigned char _array[] = {identifier, event, _value[0], _value[1]};

	checksum = _crc.xmodemChecksum(_array, 4);
	_checksum[0] = (checksum >> 8) & 0xFF; // High byte
	_checksum[1] = checksum & 0xFF;		   // Low byte

	// Set byte data array
	data[0] = identifier;
	data[1] = event;
	//data[1] = '0';
	data[2] = _value[0];
	data[3] = _value[1];
	//data[2] = '3';
	//data[3] = '3';
	data[4] = _checksum[0];
	data[5] = _checksum[1];
	// cout<<"Data 0 "<<data[0]<<endl;
	// cout<<"Data 1 "<<data[1]<<endl;
	// cout<<"Data 2 "<<data[2]<<endl;
	// cout<<"Data 3 "<<data[3]<<endl;
	// cout<<"Data 4 "<<data[4]<<endl;
	// cout<<"Data 5 "<<data[5]<<endl;

}

void SerialEvent::print()
{
	printf("[EVENT][identifier: %c][event: %d][value over serial: %d (%x) (HB: %x, LB: %x)][checksum: %x (HB: %x, LB: %x)]\r\n",
		   identifier,
		   event,
		   value, value, _value[0], _value[1],
		   checksum, _checksum[0], _checksum[1]);
}
