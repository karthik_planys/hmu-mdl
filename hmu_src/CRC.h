/*
 *	File: CRC.h
 *	Created: 11 Oct 2018, 03:27 PM
 *  Author: Vineet Upadhyay
 * 	Project: Mikros
 * 	© Planys Technologies Pvt. Ltd., Chennai
 *
 *  v1.00
 *
 * 	NOTES:
 *
 */

#ifndef CRC_H
#define CRC_H

#include <boost/crc.hpp>
#include <boost/cstdint.hpp>

class CRC
{
	private:

	public:
		CRC();
		uint16_t xmodemChecksum (unsigned char *, int);
};

#endif
