/*
 *	File: Mission.h
 *  Author: Vineet Upadhyay
 * 	Project: Mike
 * 	© Planys Technologies Pvt. Ltd., Chennai
 */

#ifndef SERIALCONNECTION_H
#define SERIALCONNECTION_H

#include "AsyncSerial.h"
#include <iostream>
#include <stdio.h>
using namespace std;

class SerialConnection 
{
private:
	CallbackAsyncSerial serial;
	string port;
	long baudrate;
	bool active;
public:
	SerialConnection();

	SerialConnection(string, long, const boost::function<void (const char *, size_t)> &);

	bool isOpen();

	void write(char);

	void close();	
};

#endif
