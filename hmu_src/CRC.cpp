/*
 *	File: CRC.cpp
 *	Created: 11 Oct 2018, 03:27 PM
 *  Author: Vineet Upadhyay
 * 	Project: Mikros
 * 	© Planys Technologies Pvt. Ltd., Chennai
 *
 *  v1.00
 *
 * 	NOTES:
 *
 */

#include "CRC.h"

// Default Constructor
CRC::CRC()
{

}

// XModem CRC Calcuation
uint16_t CRC::xmodemChecksum (unsigned char * data, int data_len)
{
	boost::crc_basic<16>  crc_ccitt1 (0x1021, 0x0000, 0, false, false);
    crc_ccitt1.process_bytes (data, data_len);
	uint16_t checksum = crc_ccitt1.checksum();
	return checksum;
}
