/*
 *	File: SerialEvent.h
 *	Created: 11 Oct 2018, 03:27 PM
 *  Author: Vineet Upadhyay
 * 	Project: Mikros
 * 	© Planys Technologies Pvt. Ltd., Chennai
 *
 *  v1.00
 *
 * 	NOTES:
 *
 */

#ifndef SERIALEVENT_H
#define SERIALEVENT_H

#include <boost/crc.hpp>
#include <boost/cstdint.hpp>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>

#include "CRC.h"
using namespace std;
class SerialEvent
{
	private:
		unsigned char identifier;
		unsigned char event;
		uint16_t value;
		unsigned char _value[2];
		uint16_t checksum;
		unsigned char _checksum[2];
		CRC _crc;

	public:
		int packet_size;
		unsigned char data[6];

		SerialEvent();
		void reset();
		void create	(unsigned char, unsigned char, uint16_t);
		//void create	(unsigned char, unsigned char, unsigned char, unsigned char);
		//void create	(unsigned char, unsigned char, unsigned char, unsigned char);
		void print();
};

#endif
