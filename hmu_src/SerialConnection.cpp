/*
 *	File: SerialConnection.cpp
 *  Author: Vineet Upadhyay
 * 	Project: Mike
 * 	© Planys Technologies Pvt. Ltd., Chennai
 * 
 *  v1.0 First Release
 *
 *	
 *	NOTES:
 *
 */ 

#include "SerialConnection.h"

using namespace std;

SerialConnection::SerialConnection()
{
}

SerialConnection::SerialConnection (string opt_port, long opt_baudrate, const boost::function<void (const char*, size_t)> &opt_callback) 
{
	this->port     = opt_port;
	this->baudrate = opt_baudrate;
	this->serial.open(this->port, this->baudrate);
	bool isOpen = this->serial.isOpen();
	//cout<<"Is serial connection open "<<isOpen<<endl;
	//printf("setting the callback \n\n");
	this->serial.setCallback(opt_callback);
}

bool SerialConnection::isOpen()
{
	return this->serial.isOpen();
}

void SerialConnection::write(char val)
{
	this->serial.write(&val, 1);
}

void SerialConnection::close()
{
	this->serial.clearCallback();
	this->serial.close();
}

