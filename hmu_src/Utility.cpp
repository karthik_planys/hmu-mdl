/*
 *	File: Utility.cpp
 *	Created: 11 Oct 2018, 01:17 AM
 *  Author: Vineet Upadhyay
 * 	Project: Mikros
 * 	© Planys Technologies Pvt. Ltd., Chennai
 *
 *  v1.00
 *
 * 	NOTES:
 *
 */

#include "Utility.h"

// Terminal Output Colors
string ANSI_RED = "\033[0;31m";
string ANSI_BOLD_RED = "\033[1;31m";
string ANSI_GREEN = "\033[0;32m";
string ANSI_BOLD_GREEN = "\033[1;32m";
string ANSI_YELLOW = "\033[0;33m";
string ANSI_BOLD_YELLOW = "\033[1;33m";
string ANSI_BLUE = "\033[0;34m";
string ANSI_BOLD_BLUE = "\033[1;34m";
string ANSI_MAGENTA = "\033[0;35m";
string ANSI_BOLD_MAGENTA = "\033[1;35m";
string ANSI_CYAN = "\033[0;36m";
string ANSI_BOLD_CYAN = "\033[1;36m";
string ANSI_WHITE = "\033[0;37m";
string ANSI_BOLD_WHITE = "\033[1;37m";
string ANSI_COLOR_RESET = "\033[0m";

// SMCU Data Queue
std::deque<char> smcu_serial_queue;

// Main Board Data Queue
std::deque<char> mbd_serial_queue;

//HMU Data Queue
std::deque<char> hmu_serial_queue;


// External Reference to Thruster Objects & IDs
// extern Thruster heaveForeThruster;
// extern Thruster heaveAftThruster;
// extern Thruster surgePortTopThruster;
// extern Thruster surgePortBottomThruster;
// extern Thruster surgeStarboardTopThruster;
// extern Thruster surgeStarboardBottomThruster;
// extern Thruster swayTopThruster;
// extern Thruster swayBottomThruster;

// extern int heaveForeThruster_ID;
// extern int heaveAftThruster_ID;
// extern int surgePortTopThruster_ID;
// extern int surgePortBottomThruster_ID;
// extern int surgeStarboardTopThruster_ID;
// extern int surgeStarboardBottomThruster_ID;
// extern int swayTopThruster_ID;
// extern int swayBottomThruster_ID;

/* Delay milliseconds */
void delay(int ms)
{
	boost::this_thread::sleep(boost::posix_time::milliseconds(ms));
}

/* Delay microseconds */
void delayMicroseconds(int ms)
{
	boost::this_thread::sleep(boost::posix_time::microseconds(ms));
}

/* Get current time in milliseconds */
long long getCurrentTimeMillis()
{
	struct timeval tv;
	gettimeofday(&tv, NULL);
	return tv.tv_sec * 1000LL + tv.tv_usec / 1000;
}

/* Close all serial handlers */
void closeSerialHandlers()
{
	// SMCUConnection.close();
	// MainBoardConnection.close();
	HMUConnection.close();
}

/* Serial callback for data received from Main Board */
// void recvFromMainBoard(const char *data, size_t len)
// {
// 	// cout << endl << ANSI_BOLD_GREEN << "[MBD]";
// 	for (int i = 0; i < len; i++)
// 	{
// 		mbd_serial_queue.push_back(data[i]);
// 		//cout << data [i];
// 	}
// 	// cout << ANSI_COLOR_RESET << endl;
// }

//  Serial callback for data received from SMCU 
// void recvFromSMCU(const char *data, size_t len)
// {
// 	// cout << endl << ANSI_BOLD_GREEN << "[SMCU]";
// 	for (int i = 0; i < len; i++)
// 	{
// 		smcu_serial_queue.push_back(data[i]);
// 		// cout << data [i];
// 	}
// 	// cout << ANSI_COLOR_RESET << endl;
// }

/* Serial callback for data received from SMCU */
void recvFromHMU(const char *data, size_t len)
{
	cout<<"Receiving Data from HMU \n\n";
	// cout << endl << ANSI_BOLD_GREEN << "[SMCU]";
	for (int i = 0; i < len; i++)
	{
		hmu_serial_queue.push_back(data[i]);
		cout <<data[i]<<endl;
	}
	// cout << ANSI_COLOR_RESET << endl;
}

/* Parse HMU Data*/

void parseHMUData(){

	printf("Parsing the HMU data in utility.cpp \n\n");
	
}

/* Parse Main Board data */
// void parseMBDData()
// {
// 	/**** Parse a data line from the serial queue ****/
// 	std::deque<char> mbd_string;
// 	int iden_count = 0;
// 	char ch;

// 	int sz = mbd_serial_queue.size();

// 	for (int i = 0; i < sz; i++)
// 	{
// 		ch = mbd_serial_queue.at(i);

// 		if (ch == '$')
// 		{
// 			iden_count++;
// 		}
// 	}

// 	if (iden_count < 2)
// 		return;
// 	else
// 	{
// 		ch = mbd_serial_queue.front();
// 		while (ch != '$')
// 		{
// 			ch = mbd_serial_queue.front();

// 			if (ch != '$')
// 			{
// 				mbd_serial_queue.pop_front();
// 			}
// 		}

// 		mbd_string.push_back(ch);
// 		mbd_serial_queue.pop_front();

// 		ch = mbd_serial_queue.front();
// 		while (ch != '$')
// 		{
// 			ch = mbd_serial_queue.front();

// 			if (ch != '$')
// 			{
// 				mbd_string.push_back(ch);
// 				mbd_serial_queue.pop_front();
// 			}
// 		}

// 		std::string data_string(mbd_string.begin(), mbd_string.end());
// 		mbd_string.clear();
// 		//cout << "\r\nMain Board String: " << data_string << endl;

// 		/**** Interpret the data line and assign to objects ****/
// 		std::vector<string> split_data = split(data_string, ",");

// 		int count = split_data.size();

// 		// String '$MBD'
// 		// cout << split_data.at(0) << endl;

// 		// Sensor Data
// 		for (int idx = 1; idx < 4; idx++)
// 		{
// 			ls_hull = stoi(split_data.at(idx++));
// 			ls_cam_encl = stoi(split_data.at(idx++));
// 			eeh_amb_temp = stoi(split_data.at(idx++));

// 			/*std::cout << "[Main Board]" << std::endl;
// 			std::cout << "Leak Sensor (Hull)			...." << ls_hull << std::endl;
// 			std::cout << "Leak Sensor (Camera Encl.)	...." << ls_cam_encl << std::endl;
// 			std::cout << "EE Hull Temp. (Ambient)		...." << eeh_amb_temp << " degC." << std::endl;*/
// 		}

// 		// Remaining Data
// 		// split_data.at (4);
// 	}
// }

// /* Parse SMCU data */
// void parseSMCUData()
// {
// 	/**** Parse a data line from the serial queue ****/
// 	std::deque<char> smcu_string;
// 	int iden_count = 0;
// 	char ch;

// 	int sz = smcu_serial_queue.size();

// 	for (int i = 0; i < sz; i++)
// 	{
// 		ch = smcu_serial_queue.at(i);

// 		if (ch == '$')
// 		{
// 			iden_count++;
// 		}
// 	}

// 	if (iden_count < 2)
// 		return;
// 	else
// 	{
// 		ch = smcu_serial_queue.front();
// 		while (ch != '$')
// 		{
// 			ch = smcu_serial_queue.front();

// 			if (ch != '$')
// 			{
// 				smcu_serial_queue.pop_front();
// 			}
// 		}

// 		smcu_string.push_back(ch);
// 		smcu_serial_queue.pop_front();

// 		ch = smcu_serial_queue.front();
// 		while (ch != '$')
// 		{
// 			ch = smcu_serial_queue.front();

// 			if (ch != '$')
// 			{
// 				smcu_string.push_back(ch);
// 				smcu_serial_queue.pop_front();
// 			}
// 		}

// 		std::string data_string(smcu_string.begin(), smcu_string.end());
// 		smcu_string.clear();
// 		//cout << "\r\nSMCU String: " << data_string << endl;

// 		/**** Interpret the data line and assign to objects ****/
// 		std::vector<string> split_data = split(data_string, ",");

// 		int count = split_data.size();

// 		// String '$SMCU'
// 		// cout << split_data.at(0) << endl;

// 		// Thruster Data
// 		for (int idx = 1; idx < 41; idx++)
// 		{
// 			int tid = stoi(split_data.at(idx++));
// 			int status = stoi(split_data.at(idx++));
// 			int speed = stoi(split_data.at(idx++));
// 			int temp = stoi(split_data.at(idx++));
// 			int rpm = stoi(split_data.at(idx));

// 			//cout << "TID: " << tid << ", STATUS (0: NA, 1: OK): " << status << ", SPEED (-32767 - 32767): " << speed << ", TEMP.: " << temp << "deg., RPM: " << rpm << endl;

// 			if (tid == heaveForeThruster_ID)
// 			{
// 				heaveForeThruster.setData(status, speed, temp, rpm);
// 			}
// 			else if (tid == heaveAftThruster_ID)
// 			{
// 				heaveAftThruster.setData(status, speed, temp, rpm);
// 			}
// 			else if (tid == surgePortTopThruster_ID)
// 			{
// 				surgePortTopThruster.setData(status, speed, temp, rpm);
// 			}
// 			else if (tid == surgePortBottomThruster_ID)
// 			{
// 				surgePortBottomThruster.setData(status, speed, temp, rpm);
// 			}
// 			else if (tid == surgeStarboardTopThruster_ID)
// 			{
// 				surgeStarboardTopThruster.setData(status, speed, temp, rpm);
// 			}
// 			else if (tid == surgeStarboardBottomThruster_ID)
// 			{
// 				surgeStarboardBottomThruster.setData(status, speed, temp, rpm);
// 			}
// 			else if (tid == swayTopThruster_ID)
// 			{
// 				swayTopThruster.setData(status, speed, temp, rpm);
// 			}
// 			else if (tid == swayBottomThruster_ID)
// 			{
// 				swayBottomThruster.setData(status, speed, temp, rpm);
// 			}
// 		}

// 		// Remaining Data
// 		// split_data.at (41);
// 	}

// 	/*std::deque<char>::iterator it, it_next, begin, end;
// 	std::deque<char> smcu_string;

// 	// find the first '$' in the queue
// 	begin = smcu_serial_queue.begin();
// 	end   = begin + smcu_serial_queue.size();
// 	it = std::find (begin, end, '$');
// 	if (it != end)
// 	{
// 		// search from the next element to find the next '$'
// 		it++;
// 		it_next = std::find (it, end, '$');
// 		if (it_next != end)
// 		{
// 			smcu_string.push_back ('$');
// 			smcu_serial_queue.erase (it-1);	
// 			while (*it != *it_next)
// 			{
// 				smcu_string.push_back (*it);
// 				smcu_serial_queue.erase (it++);
// 			}
			
// 			std::string test (smcu_string.begin(), smcu_string.end());
// 			smcu_string.clear();
// 			cout << "\r\nSMCU String: " << test << endl;
// 		}
// 		else
// 		{
// 			// Only one '$' present in queue
// 		}
// 	}*/
// }

/* Send serial event to serial handler */
void sendEvent(SerialEvent *serial_event, SerialConnection *serial_handler)
{
	printf("Sending event \n\n");
	for (int i = 0; i < serial_event->packet_size; i++)
	{
		cout<<"Data in serial event is "<<serial_event->data[i]<<endl;
		serial_handler->write(serial_event->data[i]);
	}
}

/* Encode String to BASE64 */
string base64_encode(const string &str)
{
	BIO *base64_filter = BIO_new(BIO_f_base64());
	BIO_set_flags(base64_filter, BIO_FLAGS_BASE64_NO_NL);

	BIO *bio = BIO_new(BIO_s_mem());
	BIO_set_flags(bio, BIO_FLAGS_BASE64_NO_NL);

	bio = BIO_push(base64_filter, bio);
	BIO_write(bio, str.c_str(), str.length());
	BIO_flush(bio);

	char *new_data;
	long bytes_written = BIO_get_mem_data(bio, &new_data);
	string result(new_data, bytes_written);

	BIO_free_all(bio);

	return result;
}

/* Decode String from BASE64 */
string base64_decode(const string &str)
{
	BIO *bio, *base64_filter, *bio_out;
	char inbuf[512];
	int inlen;

	base64_filter = BIO_new(BIO_f_base64());
	BIO_set_flags(base64_filter, BIO_FLAGS_BASE64_NO_NL);

	bio = BIO_new_mem_buf((void *)str.c_str(), str.length());
	bio = BIO_push(base64_filter, bio);
	bio_out = BIO_new(BIO_s_mem());

	while ((inlen = BIO_read(bio, inbuf, 512)) > 0)
	{
		BIO_write(bio_out, inbuf, inlen);
	}

	BIO_flush(bio_out);

	char *new_data;
	long bytes_written = BIO_get_mem_data(bio_out, &new_data);
	string result(new_data, bytes_written);

	BIO_free_all(bio);
	BIO_free_all(bio_out);

	return result;
}

// float getCurrentHeading()
// {
// 	return s_memory.fetch("IMU", 1);
// }

// float getCurrentPitch()
// {
// 	return s_memory.fetch("IMU", 2);
// }

// float getCurrentRoll()
// {
// 	return s_memory.fetch("IMU", 3);
// }

// float getCurrentDepth()
// {
// 	return s_memory.fetch("PressureSensor", 1) * 100;
// }

// float getCurrentAltitude()
// {
// 	return s_memory.fetch("Altimeter", 1);
// }

std::vector<string> split(const string &str, const string &delim)
{
	std::vector<string> tokens;
	size_t prev = 0, pos = 0;
	do
	{
		pos = str.find(delim, prev);
		if (pos == string::npos)
			pos = str.length();
		string token = str.substr(prev, pos - prev);
		if (!token.empty())
			tokens.push_back(token);
		prev = pos + delim.length();
	} while (pos < str.length() && prev < str.length());
	return tokens;
}

// void ThrusterCheck(Thruster *_Thruster)
// {
// 	//Thruster *_Thruster = &heaveAftThruster;
// 	long long t1 = getCurrentTimeMillis();
// 	int test_speed = 100;
// 	string status = (_Thruster->status == 1) ? "OK" : "NA";
// 	string status_color = (_Thruster->status == 1) ? ANSI_BOLD_GREEN : ANSI_BOLD_RED;
// 	string temp_color = (_Thruster->temp < 50) ? ANSI_BOLD_WHITE : ((_Thruster->temp < 65) ? ANSI_BOLD_YELLOW : ANSI_BOLD_RED);
// 	int peak_rpm = 0;
// 	while (getCurrentTimeMillis() < t1 + 5000)
// 	{
// 		peak_rpm = (_Thruster->rpm > peak_rpm) ? _Thruster->rpm : peak_rpm;
// 		_Thruster->set(test_speed);
// 		test_speed += 0;
// 		printf("\033[1;34m[%s]\033[0m %s[STATUS = %s]\033[0m %s[TEMP = %4ddeg.]\033[0m [SPEED (CS) = %4d, SPEED (ESC) = %6d] [RPM = %5d]\r",
// 			   _Thruster->getName().c_str(),
// 			   status_color.c_str(),
// 			   status.c_str(),
// 			   temp_color.c_str(),
// 			   _Thruster->temp,
// 			   test_speed,
// 			   _Thruster->esc_speed,
// 			   _Thruster->rpm);
// 		fflush(stdout);
// 		delay(10);
// 	}
// 	string result = (peak_rpm > 800) ? "SUCCESS" : "FAIL";
// 	string result_color = (peak_rpm > 800) ? ANSI_BOLD_GREEN : ANSI_BOLD_RED;
// 	printf("\033[1;34m[%s]\033[0m %s[STATUS = %s]\033[0m %s[TEMP = %4ddeg.]\033[0m [SPEED (CS) = %4d, SPEED (ESC) = %6d] [PEAK RPM = %5d] %s[%s]\033[0m\r",
// 		   _Thruster->getName().c_str(),
// 		   status_color.c_str(),
// 		   status.c_str(),
// 		   temp_color.c_str(),
// 		   _Thruster->temp,
// 		   test_speed,
// 		   _Thruster->esc_speed,
// 		   peak_rpm, result_color.c_str(), result.c_str());

// 	cout << endl;
// 	_Thruster->set(THRUSTER_SPEED_STOP_CTRL_SYS);
// }

// void autoThrusterCheck()
// {
// 	ThrusterCheck(&heaveForeThruster);
// 	ThrusterCheck(&heaveAftThruster);
// 	ThrusterCheck(&surgePortTopThruster);
// 	ThrusterCheck(&surgePortBottomThruster);
// 	ThrusterCheck(&surgeStarboardTopThruster);
// 	ThrusterCheck(&surgeStarboardBottomThruster);
// 	ThrusterCheck(&swayTopThruster);
// 	ThrusterCheck(&swayBottomThruster);
// }

/*
void autoThrusterCheck ()
{
	long long t1 = getCurrentTimeMillis ();
	int test_speed = 100;
	string status 		=	(heaveForeThruster.status == 1) ? "OK" : "NA";
	string status_color	=	(heaveForeThruster.status == 1) ? ANSI_BOLD_GREEN : ANSI_BOLD_RED;
	string temp_color	=	(heaveForeThruster.temp < 50) ? ANSI_BOLD_WHITE : ((heaveForeThruster.temp < 65) ? ANSI_BOLD_YELLOW : ANSI_BOLD_RED);
	int peak_rpm = 0;
	while (getCurrentTimeMillis () < t1 + 5000)
	{		
		peak_rpm = (heaveForeThruster.rpm > peak_rpm) ? heaveForeThruster.rpm : peak_rpm;
		heaveForeThruster.set (test_speed);
		test_speed += 0;
		printf ("\033[1;34m[%s]\033[0m %s[STATUS = %s]\033[0m %s[TEMP = %4ddeg.]\033[0m [SPEED (CS) = %4d, SPEED (ESC) = %6d] [RPM = %5d]\r", 
				heaveForeThruster.getName().c_str(),				
				status_color.c_str(),
				status.c_str(), 
				temp_color.c_str(),
				heaveForeThruster.temp,
				test_speed,
				heaveForeThruster.esc_speed,
				heaveForeThruster.rpm);
		fflush (stdout);
		delay (10);
	}
	string result 		= (peak_rpm > 800) ? "SUCCESS" : "FAIL";
	string result_color	= (peak_rpm > 800) ? ANSI_BOLD_GREEN : ANSI_BOLD_RED;
	printf ("\033[1;34m[HEAVE FORE THRUSTER]\033[0m %s[STATUS = %s]\033[0m %s[TEMP = %4ddeg.]\033[0m [SPEED (CS) = %4d, SPEED (ESC) = %6d] [PEAK RPM = %5d] %s[%s]\033[0m\r", 
			status_color.c_str(),
			status.c_str(), 
			temp_color.c_str(),
			heaveForeThruster.temp,
			test_speed,
			heaveForeThruster.esc_speed,
			peak_rpm, result_color.c_str(), result.c_str());

	cout << endl;
	heaveForeThruster.set (THRUSTER_SPEED_STOP_CTRL_SYS);
}*/
