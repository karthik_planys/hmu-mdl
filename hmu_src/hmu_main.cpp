#include "hmu.h"

void cli()
{
	printf("Reading CLI \n\n");
	int opt = 0, opt_idx = 0;
	
	do
	{
		opt_idx = 0;
		printf("\r\n");
		printf("[%d]  Turn OFF IBC \n", opt_idx++);
		printf("[%d]  Turn ON IBC \n", opt_idx++);
		printf("[%d]  Clear Fault \n", opt_idx++);
		printf("[%d]  Set VOUT Command \n", opt_idx++);
		printf("[%d]  Set VTRIM \n", opt_idx++);
		printf("[%d]  Restore Default Configuration \n", opt_idx++);
		printf("[%d]  Save Current Configuration \n", opt_idx++);
		printf("[%d]  Get IBC Data String \n", opt_idx++);
		printf("[%d]  Get HMU Data String \n", opt_idx++);
		printf("[%d]  Exit \n", opt_idx++);
		printf("Enter an option ....\n");
		scanf("%d", &opt);
		printf("\n");

		switch (opt)
		{
			unsigned char ibc_input;
			case 0:
			{
				printf("Turn OFF IBC \n");
				printf("Port choices \n");
				printf("0. All the IBCs \n");
        		printf("1. IBC-1 \n");
        		printf("2. IBC-2 \n");
        		printf("3. IBC-3 \n");
        		printf("4. IBC-4 \n");
        		printf("Enter the port choice \n");
        		scanf(" %d", &ibc_input);
        		printf("Turning OFF %c IBC ", ibc_input);
        		hmu_event.create(HMU_IDENTIFIER, '0', ibc_input);
        		sendEvent(&hmu_event,&HMUConnection);
        		delay(HMU_PACKET_DELAY_MS);
				break;
			}
			case 1:
				printf("Turn ON IBC \n");
				printf("Port choices \n");
				printf("0. All the IBCs \n");
        		printf("1. IBC-1 \n");
        		printf("2. IBC-2 \n");
        		printf("3. IBC-3 \n");
        		printf("4. IBC-4 \n");
        		printf("Enter the port choice \n");
        		scanf(" %d", &ibc_input);
        		printf("Turning ON %c IBC ", ibc_input);
        		hmu_event.create(HMU_IDENTIFIER,'1',ibc_input);
        		sendEvent(&hmu_event,&HMUConnection);
        		delay(HMU_PACKET_DELAY_MS);
				break;
			case 2:
				printf("Clearing Faults \n");
				hmu_event.create(HMU_IDENTIFIER,'2','0');
        		sendEvent(&hmu_event,&HMUConnection);
        		delay(HMU_PACKET_DELAY_MS);
				break;
			case 3:
			{
				printf("Set VOUT command \n");
				printf("enter voltage command value (9 to 14 V float values included )");
				float volt;
				scanf("%f", &volt);
				int voltdata = int(volt * 512);
				cout<<"voltdata is "<<voltdata<<endl;
				int sendData = voltdata;
				cout<<"send data is "<<sendData<<endl;

				std::stringstream stream;
				stream << std::hex << sendData;
				std::string result(stream.str());
				cout<<"Hex value is "<<result<<endl;
				
				const char* valChar = result.c_str();
    			string str1;
    			str1 += "0x";
    			str1 += valChar[0];
    			str1 += valChar[1];
    			string str2;
    			str2 += "0x";
    			str2 += valChar[2];
    			str2 += valChar[3];
				int sendByte2 = int(sendData/256);
				cout<<"sendByte2 is "<<sendByte2<<endl;
				int sendByte1 = sendData%256;
				cout<<"sendByte1 is "<<sendByte1<<endl;
				// printf("Enter Hex1 \n");
				// scanf("%c", &hex1);
				// printf("Enter Hex2 \n");
				// scanf("%c", &hex2);
				//hmu_event.create(HMU_IDENTIFIER,'3',str1,str2);
        		//sendEvent(&hmu_event,&HMUConnection);
        		//delay(HMU_PACKET_DELAY_MS);
				break;
			}
			case 4:
				printf("Set VTRIM \n");
				hmu_event.create(HMU_IDENTIFIER,'4',12);
        		sendEvent(&hmu_event,&HMUConnection);
        		delay(HMU_PACKET_DELAY_MS);
				break;
			case 5:
				printf("Restore Defaults \n");
				hmu_event.create(HMU_IDENTIFIER,'5',12);
        		sendEvent(&hmu_event,&HMUConnection);
        		delay(HMU_PACKET_DELAY_MS);
				break;
			case 6:
				printf("Save Configuration \n");
				hmu_event.create(HMU_IDENTIFIER,'6',12);
        		sendEvent(&hmu_event,&HMUConnection);
        		delay(HMU_PACKET_DELAY_MS);
				break;
			case 7:
				printf("IBC String \n");
				hmu_event.create(HMU_IDENTIFIER,'<',32);
        		sendEvent(&hmu_event,&HMUConnection);
        		delay(HMU_PACKET_DELAY_MS);
				break;
			case 8:
				printf("HMU Data String \n");
				hmu_event.create(HMU_IDENTIFIER,'=',23);
        		sendEvent(&hmu_event,&HMUConnection);
        		delay(HMU_PACKET_DELAY_MS);
				break;
			default:
				break;
			
		}
		printf("Parse the data \n\n");
		parseHMUData();
	} while (opt != (opt_idx - 1));
}

int main(){

	printf("Starting CLI \n\n");
	cli();
}
