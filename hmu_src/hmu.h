#ifndef HMU_H
#define HMU_H
#include "SerialConnection.h"
#include "SerialEvent.h"
#include <boost/thread.hpp>
#include <cmath>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <iomanip>
#include "Utility.h"

// SMCU Serial Connection Parameters
#define HMU_SERIAL_PORT				"/dev/ttyACM0"
#define HMU_BAUD_RATE					115200
#define HMU_IDENTIFIER					'&'
#define HMU_PACKET_DELAY_MS			4
#define HMU_DATA_REQUEST_TIMEOUT_MS	250

/* Serial Connections */
SerialConnection HMUConnection(HMU_SERIAL_PORT, HMU_BAUD_RATE, recvFromHMU);
SerialEvent hmu_event;


#endif
