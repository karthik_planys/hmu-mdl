from crccheck.crc import CrcXmodem as crcx
import serial
import time
CRC = crcx()
flag = 0
chflag = 0
while flag == 0:
    print('Choices :')
    print('0. Turn OFF IBC ')
    print('1. Turn ON IBC ')
    print('2. Clear Fault ')
    print('3. Set Vout command ')
    print('4. Set Vtrim ')
    print('5. Restore default configurations')
    print('6. Save current configurations')
    print('7. Get IBC data string')
    print('8. Get HMU data string')
    print('9 & else. Exit')
    ch = input('Enter choice number')
    if (ch == '0'):
        print("Turn OFF IBC")
        print('Port choices :')
        print('0. All the IBCs')
        print('1. IBC-1')
        print('2. IBC-2')
        print('3. IBC-3')
        print('4. IBC-4')
        port = input('Enter the port choice')
        pb = b'xx'
        if (port == '1'):
            pb = b'11'
        elif (port == '2'):
            pb = b'22'
        elif (port == '3'):
            pb = b'33'
        elif (port == '4'):
            pb = b'44'
        else:
            pb = b'00'
        print("pb value is ",pb)
        data = b'&0' + pb
    elif(ch == '1'):
        print("Turn ON")
        print('Port choices :')
        print('0. All the IBCs')
        print('1. IBC-1')
        print('2. IBC-2')
        print('3. IBC-3')
        print('4. IBC-4')
        port = input('Enter the port choice')
        if (port == '1'):
            pb = b'11'
        elif (port == '2'):
            pb = b'22'
        elif (port == '3'):
            pb = b'33'
        elif (port == '4'):
            pb = b'44'
        else:
            pb = b'00'

        data = b'&1'+ pb
    elif(ch == '2'):
        print("Clearing Faults")
        data = b'&212'
    elif(ch == '3'):
        print("SET VOUT COMMAND")
        volt = float(input('enter voltage command value (9 to 14 V- float values included '))
        voltdata = int(volt*512)
        senddata = voltdata
        sendbyte2 = int(senddata/256)
        sendbyte1 = senddata%256
        print('Byte 1 = ' + str(sendbyte1))
        print('Byte 2 = ' + str(sendbyte2))
        print('Data = ' + str(senddata))
        data = b'&3'+bytes(chr(sendbyte2), 'latin-1')+bytes(chr(sendbyte1), 'latin-1')
    elif(ch == '4'):
        print('SET VTRIM')
        volt = float(input('enter voltage trim value (-4 to 4 V - float values included '))
        port = int(input('enter IBC no. (1-4)'))-1
        voltdata = int(volt*512)
        if(voltdata < 0):
            voltdata = 16384 +voltdata
        senddata = port*16384+voltdata
        sendbyte2 = int(senddata/256)
        sendbyte1 = senddata%256
        print('Byte 1 = ' + str(sendbyte1))
        print('Byte 2 = ' + str(sendbyte2))
        print('data = '+str(senddata))
        data = b'&4'+bytes(chr(sendbyte2), 'latin-1')+bytes(chr(sendbyte1), 'latin-1')
    elif(ch == '5'):
        print("Restore Defaults")
        data = b'&512'
    elif(ch == '6'):
        print("Save Configuration")
        data = b'&612'
    elif(ch == '7'):
        print("IBC String")
        #data = b'&'+bytes(chr(60), 'utf-8')+b'32'
        data = b'&'+bytes(chr(60).encode('utf-8'))+b'32'
    elif(ch == '8'):
        print("HMU Data String")
        data = b'&=23'
    else:
         flag = 1
         print('exiting the loop')
         break


    crcarray = CRC.calcbytes(data)
    #print("data is ",data)
    #print("Crc array is ",crcarray)
    data = data + crcarray
    #print(data)
    #print("CRC Check Result =", CRC.calcbytes(data))
    #print('serial communication : ')
    with serial.Serial('/dev/ttyACM0', 115200, timeout=1) as ser:
        #print("transmission data = ", data)
        ser.write(data)
        recvstr = ""
        flag2 = 0
        while flag2 == 0:
            time.sleep(1)
            if (ser.inWaiting() > 0):
                recvstr = ser.readline()
                if (len(recvstr) != 0):
                    print("=====================")
                    print("Output received from IBC")
                    print(recvstr.decode('ascii'))
                    print("=====================")
            else:
                flag2 = 1
                print("Serial communication complete")





