from crccheck.crc import Crc32, CrcXmodem
from crccheck.checksum import Checksum32

# Quick calculation
data = bytearray.fromhex("DEADBEEF")
crc = Crc32.calc(data)
checksum = Checksum32.calc(data)

# Procsss multiple data buffers
data1 = b"Binary string"  # or use .encode(..) on normal sring - Python 3 only
data2 = bytes.fromhex("1234567890")  # Python 3 only, use bytearray for older versions
data3 = (0x0, 255, 12, 99)  # Iterable which returns ints in byte range (0..255)
crcinst = CrcXmodem()
crcinst.process(data1)
crcinst.process(data2)
crcinst.process(data3[1:-1])
crcbytes = crcinst.finalbytes()
print("crc bytes ", crcbytes)
crchex = crcinst.finalhex()
print("crc hex ",crchex)
crcint = crcinst.final()
print("crcint ",crcint)