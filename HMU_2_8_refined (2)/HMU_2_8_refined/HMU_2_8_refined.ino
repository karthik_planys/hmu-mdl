#include <FastCRC.h>
#include "i2cmaster.h"
#include <avr/wdt.h>
#include <Servo.h>
#include "config.h"
#include "protocol.h"
#include "utility.h"
#include "QueueArray.h"

// Queue for receiving and processing instructions
QueueArray <unsigned char> cpu_serial_queue;
QueueArray <Instruction> instructions_queue;

// 16-bit CRC check object
FastCRC16 CRC16;

// I2C Addresses of the IBCs in use
byte port[] = {0x09, 0x12, 0x1B, 0x78};
int portcount = 4;

// IBC Array object
IBC IBC_Array[] = {IBC(port[0]), IBC(port[1]), IBC(port[2]), IBC(port[3])};

void setup()
{
  int e;
  // Initialize CPU Serial Communication
  CPU_SERIAL.begin(CPU_SERIAL_BAUD_RATE);
  while (!CPU_SERIAL);
  CPU_SERIAL.println ("HMU BOOT, Firmware v1.3, Modification Date: 2 December 2020");

  // Initialize I2C Communication
  i2c_init();

  // Enabling IBCs over external digital logic
  PRI_EN1.setHigh();
  PRI_EN2.setHigh();
  PRI_EN3.setHigh();
  PRI_EN4.setHigh();

  //i2c_scanner();                                  /* Optional scanning */

  // Initialize IBCs
  IBC_initialize();

  // Starting Fan
  fan.setHigh();

  // Blink Status LED to output proper initialization
  status_led_blink(5, 1000);

  // Enable Watchdog timer
  //   wdt_enable(WDTO_2S);
}

void loop()
{
  // Check serial buffer for incoming data and instructions
  checkForSerialReceive();

  // Process the pending instruction queue
  // Must not spend a lot of time here to as to have a serial buffer overflow
  processInstructionsQueue();

  // Check status of CPU
  checkCPUStatus();
  // Watchdog reset (to be executed every 2 seconds)
  //  wdt_reset();

}

void checkCPUStatus()
{
  // CPU Connection Lost
  if (millis() - last_instruction_timestamp > CPU_ALIVE_TIMEOUT)
  {
    //stopAllDevices();
    status_led_blink(1, 200);
  }
}
