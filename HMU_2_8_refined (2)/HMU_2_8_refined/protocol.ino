#ifndef _PROTOCOL_INO
#define _PROTOCOL_INO

#include "protocol.h"
#include "utility.h"

Instruction::Instruction()
{
  identifier    = NULL_VAL;
  event         = NULL_VAL;
  value[0]      = NULL_VAL;
  value[1]      = NULL_VAL;
  checksum[0]   = NULL_VAL;
  checksum[1]   = NULL_VAL;
}
int flag = 0;
void checkForSerialReceive()
{
  int _buf_count = CPU_SERIAL.available();
  char ch;
  if (_buf_count > 0)
  {
    while (_buf_count > 0)
    {
      ch = CPU_SERIAL.read();
      cpu_serial_queue.push(ch);
      _buf_count--;
    }
  }
 /*if (millis() - lastmillis > 1000)
  {
    lastmillis = millis();
    if (flag == 0)
    {
      flag = sendIBCDataString();     // Transmit IBC String every second
    }
    else
    {
      //    I2C_Recovery();
      for (int i = 0; i < portcount; i++) //Attempting recovery
        IBC_Array[i].comm_flag = 0;
      flag = 0;
    }
  }*/
  parseInstructions();
}

void parseInstructions()
{
  if (cpu_serial_queue.count() == 0)
  {
    return;
  }

  if (cpu_serial_queue.count() >= PACKET_SIZE)
  {
    while (cpu_serial_queue.count() >= PACKET_SIZE)
    {
      trimQueue(&cpu_serial_queue, '&');

      if (cpu_serial_queue.count() >= PACKET_SIZE)
      {
        getInstructions();
      }
    }
  }
  else
  {
    return;
  }
}

void trimQueue(QueueArray <unsigned char> *queue, unsigned char identifier)
{
  if (queue->count() == 0)
  {
    return;
  }

  if (queue->front() != identifier)
  {
    while (queue->front() != identifier)
    {
      queue->dequeue();

      if (queue->count() == 0)
      {
        return;
      }
    }
  }
}

void getInstructions()
{
  Instruction temp_instruction;
  temp_instruction.identifier   = cpu_serial_queue.dequeue();
  temp_instruction.event        = cpu_serial_queue.dequeue();
  temp_instruction.value[0]     = cpu_serial_queue.dequeue();
  temp_instruction.value[1]     = cpu_serial_queue.dequeue();
  temp_instruction.checksum[0]  = cpu_serial_queue.dequeue();
  temp_instruction.checksum[1]  = cpu_serial_queue.dequeue();

  byte crc_array[PACKET_SIZE] = {temp_instruction.identifier,
                                 temp_instruction.event,
                                 temp_instruction.value[0],
                                 temp_instruction.value[1],
                                 temp_instruction.checksum[0],
                                 temp_instruction.checksum[1]
                                };


  // Perform CRC Check
  if (CRC_CHECK_ENABLED)
  {
    if (CRCCheck(crc_array, PACKET_SIZE))
    {
      instructions_queue.push(temp_instruction);
    }
    else
    {
      //checksum fail
      CPU_SERIAL.println("Checksum Fail");
    }
  }
  else
  {
    instructions_queue.push(temp_instruction);
  }
}

void processInstructionsQueue()
{
  if (instructions_queue.count() > 0)
  {
    while (instructions_queue.count() > 0)
    {
      Instruction instruction = instructions_queue.dequeue();

      executeInstruction(instruction);
    }
  }
}

void executeInstruction (Instruction _instruction)
{
  unsigned int data;
  int port_id;
  float volt, vtrim, vout;
  int sum = 0;

  switch (_instruction.event)
  {
    case EVT_TURN_OFF:                                                // Turning off (individually or altogether)
      port_id = _instruction.value[0];
      if (port_id == '0')
      {
        CPU_SERIAL.println("Turning off all IBCs..");
        for (int i = 0; i < portcount; i++)
        {
          IBC_Array[i].turnoff();
          CPU_SERIAL.println(String("IBC-" + String(i + 1) + " OFF"));
        }
      }
      else if (port_id > '0' && port_id <= '4')
      {
        IBC_Array[port_id - '0' - 1].turnoff();
        CPU_SERIAL.println(String("IBC-" + String(port_id - '0') + " OFF"));
      }
      else
        CPU_SERIAL.println("Incorrect port_id");
      break;

    case EVT_TURN_ON:                                                   // For turning on (individually or altogether)
      port_id = _instruction.value[0];
      if (port_id == '0')
      {
        CPU_SERIAL.println("Turning on all IBCs...");
        for (int i = 0; i < portcount; i++)
        {
          IBC_Array[i].turnon();
          CPU_SERIAL.println(String("IBC-" + String(i + 1) + " turned on"));
        }
      }
      else if (port_id > '0' && port_id <= '4')
      {
        IBC_Array[port_id - '0' - 1].turnon();
        CPU_SERIAL.println(String("IBC-" + String(port_id - '0') + " turned on"));
      }
      else
        CPU_SERIAL.println("Incorrect port_id");
      break;

    case EVT_CLEAR_FAULTS:                                                      // Clearing faults & warnings (altogether)
      for (int i = 0; i < portcount; i++)
      {
        IBC_Array[i].clear_fault();
        CPU_SERIAL.println(String("IBC-" + String(i + 1) + " fault cleared"));
      }
      break;

    case EVT_SET_VOUT:                                                          // Changing Vout command of the IBCs (altogether)
      data = _instruction.value[1] * 256 + _instruction.value[0];
      volt = get_volt(data);
      for (int i = 0; i < portcount; i++)
      sum += IBC_Array[i].comm_flag;

      if(sum > 0)
        CPU_SERIAL.println("IBC with I2C problem detected. Vout command cannot be executed");
      else
      {
      for (int i = 0; i < portcount; i++)
      {
        vtrim = IBC_Array[i].get_vtrim();
        if (volt > 13.2)
          CPU_SERIAL.println("Value greater than maximum Vout");
        else if (volt < 8.2)
          CPU_SERIAL.println("Value lesser than minimum Vout");
        else
        {
          IBC_Array[i].set_vout(volt);
          CPU_SERIAL.println("IBC-" + String(i + 1) + ":Vout_Command=" + String(IBC_Array[i].get_vout_command()) + ";Vout=" + String(IBC_Array[i].get_vout()));
        }
      }
      }
      break;

    case EVT_SET_VTRIM:                                                           // Changing Vtrim for fine tuning of the IBC output voltage (individual)
      data = _instruction.value[1] * 256 + _instruction.value[0];
      port_id = get_portID(data);
      vtrim = get_volt(data);
      vout = IBC_Array[port_id].get_vout();
      if(IBC_Array[port_id].comm_flag)
        CPU_SERIAL.println("IBC has I2C communication problem");
      else
      {
      if (vout + vtrim > 13.2)
        CPU_SERIAL.println("Value greater than maximum Vout");
      else if (vout + vtrim < 8.0)
        CPU_SERIAL.println("Value lesser than minimum Vout");
      else
      {
        IBC_Array[port_id].set_vtrim(vtrim);
        CPU_SERIAL.println("IBC-" + String(port_id + 1) + ":Vtrim_Command=" + String(IBC_Array[port_id].get_vtrim()) + ";Vout=" + String(IBC_Array[port_id].get_vout()));
      }
      }
      break;

    case EVT_RESTORE_DEFAULT:                                                     // For restoring factory settings of the IBC
      for (int i = 0; i < portcount; i++)
      {
        IBC_Array[i].comm_flag = 0;
        IBC_Array[i].restore_default();
        if(IBC_Array[i].comm_flag)
          CPU_SERIAL.println("I2C problem @ IBC-"+String(i+1));
      }
      CPU_SERIAL.println("Default settings restored");
      break;

    case EVT_SAVE_CONFIGURATION:                                                   // For saving the user configurations of the IBCs
      for (int i = 0; i < portcount; i++)
      {
        IBC_Array[i].store_user();
        if(IBC_Array[i].comm_flag)
         CPU_SERIAL.println("I2C problem detected @ IBC-"+String(i+1));
      }
      CPU_SERIAL.println("User settings saved");
      break;

    case EVT_IBC_STRING:                                                           // Transmits the IBC data alongwith the HMU String (Backplane data)
      sendIBCDataString();
      break;

    case EVT_REQUEST_DATA_STRING:                                                  // HMU string (BACKPLANE state) to be sent separately
      // CPU Data Request
      createHMUDataString ();

      sendHMUDataString ();

      break;

    case EVT_SET_DEBUG_LEVEL:
      // Set Debug Level
      debug_level     = ((0x0000 | _instruction.value[0]) << 8) | _instruction.value[1];

      if (debug_level == DEBUG_ALL)
      {
        DEBUG_SERIAL.print (DEBUG_IDENTIFIER);
        DEBUG_SERIAL.print ("Debug Level Toggled Successfully to DEBUG_ALL\r\n");
      }
      else if (debug_level == DEBUG_NONE)
      {
        DEBUG_SERIAL.print (DEBUG_IDENTIFIER);
        DEBUG_SERIAL.print ("Debug Level Toggled Successfully to DEBUG_NONE\r\n");
      }
      else
      {
        DEBUG_SERIAL.print (DEBUG_IDENTIFIER);
        DEBUG_SERIAL.print ("Invalid Debug Level\r\n");
      }

      break;
    case EVT_SPEED_TEST:
      // Speed Test
      _sample_size    = ((0x0000 | _instruction.value[0]) << 8) | _instruction.value[1];

      if (_packet_counter == 0)
      {
        t0 = millis();
        _packet_counter++;
      }
      else if (_packet_counter == _sample_size - 1)
      {
        tn = millis();

        long dt = tn - t0;
        Serial.print("Speed Test, ");
        Serial.print(_sample_size);
        Serial.print(" Packets x 6 Bytes, Duration: ");
        Serial.print(dt);
        Serial.println(" ms");

        _packet_counter = 0;
      }
      else
      {
        _packet_counter++;
      }
      break;
    case 254:
      // Dummy Packet to Keep CPU Alive
      break;
    default:
      break;
  }
}

#endif
