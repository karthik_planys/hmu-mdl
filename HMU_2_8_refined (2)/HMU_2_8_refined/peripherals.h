#ifndef _PERIPHERALS_H
#define _PERIPHERALS_H

#include "config.h"
#include<Servo.h>
// Servo Control Mode For Class 'PWMDevice'

#include "i2cmaster.h"
//  I2C communication library for IBC Communication

#define SERVO_MODE_ANGLE                1
#define SERVO_MODE_MICROSECONDS         2

/*
    Class IBC
*/
class IBC
{
  public:
    IBC(byte);
    byte lowbyte;
    byte highbyte;
    byte comm_flag;
    float last_vout;
    void set_vout(float);
    void set_vtrim(float);
    float get_vout();
    float get_vout_command();
    float get_vtrim();
    float get_vin();
    float get_iin();
    float get_temp();
    float get_pout();
    float get_pin();
    float get_iout();
    float get_mfr_vout_min();
    float get_mfr_vout_max();
    void set_vtrim();
    byte get_status(byte);
    unsigned int get_status_word();
    String get_status_data();
    void store_user();
    void restore_default();
    void clear_fault();
    void turnoff();
    void turnon();
    float get_9expdata(byte);
    float get_expdata(byte);

  private:
    byte _port;
    byte _pecbyte1;
    byte _pecbyte2;
    byte _CRCbuff[20];
    byte _messagebuff[20];
    void clearbuff(void);
    void printMessagebuff(int);
    void printCRCbuff(int);
    void get_data(byte, int);
    byte getPECbyte(int);
    void send_command(byte);
    void set_byte(byte, byte);
    void set_word(byte, byte, byte);

    void get_setcommand(float, int);
    void get_setvoltcommand(float);
};

/*
  Class PWMDevice
*/
class PWMDevice
{
  private:
    int pin;
    int signal;
    int servo_mode;
    int pulse_upper_lim, pulse_lower_lim;
    Servo servo_device;
  public:
    PWMDevice();
    void init(int, int, int, int, int);
    void stop();
    void setValue(int);
    int getValue();
};

/*
  Class manipulator
*/
class manipulator
{
  private:
    int timer;
    int direction_pin;
    int signal_pin;
    int speed;
  public:
    manipulator();
    void init(int, int, int);
    void stop();
    void run(int);
    void setForward();
    void setReverse();
    void setSpeed(int);
    int getSpeed();
};

/*
  Class analogSensor
*/
class analogSensor
{
  private:
    int pin, mode;
    int value;
  public:
    analogSensor();
    analogSensor(int, int);
    void init(int, int);
    int getValue();
    void setValue(int);
    int readPin();
    int readAverage(int);
};

/*
  Class digitalSensor
*/

class digitalSensor
{
  private:
    int pin, mode;
    bool high, low;
  public:
    digitalSensor();
    digitalSensor(int, int);
    void init(int, int);
    void setHigh();
    void setLow();
    void toggle();
    bool isHigh();
    bool isLow();
    int  readPin();
};

#endif
