#ifndef _CONFIG_H
#define _CONFIG_H

#include "peripherals.h"
#include "protocol.h"

#define byte uint8_t

/*
 * Firmware Configuration
 */
#define CRC_CHECK_ENABLED                true

/*
 * Communication Parameters
 */
// I2C Frequency
#define TWI_FREQ                         400000

// Primary Communication Channel with CPU
#define CPU_SERIAL                       Serial
#define CPU_SERIAL_BAUD_RATE             115200
bool cpu_alive                           = true;
unsigned long lastmillis                 = millis();
/*
 * Status LED
 */
#define STATUS_LED_PIN                   A0
digitalSensor status_led(STATUS_LED_PIN, OUTPUT);

/* DPU state reading */
#define T_ALARM                          5
digitalSensor t_alarm_acdc(T_ALARM, INPUT);

#define DC_OK                           13
digitalSensor dc_ok_acdc(DC_OK, INPUT);

#define ACDC_EN                         10   // Pin can be connected to Vtrim port of the backplane for turning ON/OFF of the DPU 3200 AC/DC power supply (Not connected presently) 
digitalSensor acdc_onoff(ACDC_EN, OUTPUT);

/* Cooling Fans */
#define FAN_EN                          A1
digitalSensor fan(FAN_EN, OUTPUT);

/* Priority Enable pins of IBC */
#define IBC1_EN                         11
#define IBC2_EN                         SCK
#define IBC3_EN                         MOSI
#define IBC4_EN                         MISO
digitalSensor PRI_EN1(IBC1_EN, OUTPUT);
digitalSensor PRI_EN2(IBC2_EN, OUTPUT);
digitalSensor PRI_EN3(IBC3_EN, OUTPUT);
digitalSensor PRI_EN4(IBC4_EN, OUTPUT);

/* IBC Power Good digital pins */
#define IBC2_PG                         A2
#define IBC3_PG                         A3
#define IBC4_PG                         A4
#define IBC_PG                          A5
digitalSensor PG2(IBC2_PG, INPUT);
digitalSensor PG3(IBC3_PG, INPUT);
digitalSensor PG4(IBC4_PG, INPUT);
digitalSensor PG(IBC_PG, INPUT);

byte I2C_clear = 1;

float operating_voltage       =       13.2;

/*
 * Temperature Sensor
 */
#define NTC_1                            A9
#define NTC_2                            A10
#define Ambient_Temp_Sense_Pin           A11

float Ambient_Temperature     =        0.0;
float temperature1            =        0.0;
float temperature2            =        0.0;
byte transmitbyte             =          0;

//Temperature Sensor Objects
analogSensor NTC1(NTC_1, INPUT);
analogSensor NTC2(NTC_2, INPUT);
analogSensor Ambient_Temp_Sensor(Ambient_Temp_Sense_Pin, INPUT);

//NTC Parameters
#define THERMISTORNOMINAL                10000
#define TEMPERATURENOMINAL               25
#define BCOEFFICIENT                     3400
#define SERIESRESISTOR                   3300

/*
 * Leak Sensor
 */
#define HULL_LEAK_SENSOR_PIN             9
#define CAM_ENC_LEAK_SENSOR_PIN          4

int HULL_LEAK_STATUS           =         0;
int CAM_ENC_LEAK_STATUS        =         0;

digitalSensor Hull_Leak_Sensor(HULL_LEAK_SENSOR_PIN, INPUT);
digitalSensor Cam_Enc_Leak_Sensor(CAM_ENC_LEAK_SENSOR_PIN, INPUT);

#endif
