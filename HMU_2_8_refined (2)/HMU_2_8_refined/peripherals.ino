#ifndef _PERIPHERALS_INO
#define _PERIPHERALS_INO

#include "peripherals.h"
#include <Arduino.h>
#include <Wire.h>
#include "i2cmaster.h"

/*
   Class IBC
*/

IBC::IBC(byte port)     // constructor
{
  _port = port;     //set i2c port address as the object's feature
  comm_flag = 0;
  last_vout = 0.0;
}

void IBC::clearbuff(void)  // Clears the CRC and message buffer.
{
  for (int i = 0; i < 20; i++)
  {
    _CRCbuff[i] = 0;
    _messagebuff[i] = 0;
  }
}


byte IBC::getPECbyte(int len)       // Function to calculate PEC byte using CRC8 algorithm using data from CRC calculation buffer
{
  const byte generator = 0x07;
  byte crc = 0; /* start with 0 so first byte can be 'xored' in */
  byte currByte;
  for (int i = 0; i < len; i++)
  {
    currByte = _CRCbuff[i];
    crc ^= currByte; /* XOR-in the next input byte */
    for (int i = 0; i < 8; i++)
    {
      if ((crc & 0x80) != 0)
      {
        crc = (byte)((crc << 1) ^ generator);
      }
      else
      {
        crc <<= 1;
      }
    }
  }
  return crc;
}

void IBC::get_data(byte command, int len)  // Read data from IBC
{
  int count = 0;
  clearbuff(); // Clear CRC calculation buffer and message buffer

  comm_flag = i2c_start(_port * 2 + I2C_WRITE);
  if (!comm_flag)
  {
    _CRCbuff[count++] = _port * 2;    // starting frame = i2c_address<<1 + Write(=0)

    i2c_write(command);   // register address for the value to be queried

    _CRCbuff[count++] = command;      // update CRC calculation buffer

    i2c_rep_start(_port * 2 + I2C_READ); // change the wire to read

    _CRCbuff[count++] = _port * 2 + 1; // last transmission frame from host = i2c_address << 1 + Read(=1)
    for (int i = 0; i < len; i++)
    {
      _messagebuff[i] = i2c_readAck();        // Receive data from IBC and store in message buffer
      _CRCbuff[count++] = _messagebuff[i];  // update CRC buffer accordingly
    }
    _messagebuff[len] = i2c_readNak();

    comm_flag = i2c_stop();
    if (!comm_flag)
    {
      _pecbyte1 = _messagebuff[len];        // read PEC byte from IBC
      _pecbyte2 = getPECbyte(count);        // calculate PEC byte for the frameset
      if (_pecbyte1 == _pecbyte2)         // if received PEC = calculated PEC then Proper Communication ensured (commOk = 1)
        comm_flag = 0;
      else
        comm_flag = 1;
      lowbyte = _messagebuff[0];          // storing lower byte data
      highbyte = _messagebuff[1];         // storing higher byte data
    }
  }
  else
  {
    comm_flag = 1;
  }
}


byte IBC::get_status(byte command)              // used to obtain CML, Temperature, Vout, Iout or Input Status byte
{
  get_data(command, 1);                         // obtain single byte data
  return (_messagebuff[0]);
}


unsigned int IBC::get_status_word()            // Used to obtain status word
{
  get_data(0x79, 2);
  return (_messagebuff[0] + _messagebuff[1] * 256);
}

String IBC::get_status_data()                 // Used to get the interpreted meaning of the Status word
{
  unsigned int status_word = get_status_word();
  byte bitvalue;
  String str = "";
  if (status_word == 0)
    str = "All Clear";
  else
  {
    int first = 1;
    for (int i = 0; i < 16; i++)
    {
      bitvalue = (status_word >> i) & 1;
      if (bitvalue == 1)
      {
        if (first)
          first = 0;
        else
          str = str + ",";
        switch (i)
        {
          case 0:
            str = str + "Unlisted";
            break;
          case 1:
            str = str + "CML";
            break;
          case 2:
            str = str + "temp";
            break;
          case 3:
            str = str + "Vin_UV";
            break;
          case 4:
            str = str + "Iout_OC";
            break;
          case 5:
            str = str + "Vout_OV";
            break;
          case 6:
            str = str + "off";
            break;
          case 7:
            str = str + "busy";
            break;
          case 8:
            str = str + "unlisted";
            break;
          case 9:
            str = str + "other";
            break;
          case 10:
            str = str + "fans";
            break;
          case 11:
            str = str + "PG";
            break;
          case 12:
            str = str + "MFR";
            break;
          case 13:
            str = str + "INPUT";
            break;
          case 14:
            str = str + "Iout/Pout";
            break;
          case 15:
            str = str + "Vout";
            break;
        }
      }
    }
  }
  return (str);
}
float IBC::get_9expdata(byte command)         // get values for parameters by multiplying with 2^-9 (Output voltage parameters)
{
  byte y, x;
  get_data(command, 2);                       // acquire data
  x = _messagebuff[0];                        // separate MSB and LSB
  y = _messagebuff[1];

  int c = (y * 256 + x);                      // calculate mantissa
  float vol = c * float(pow(2, -9));          // calculate corresponding value
  return (vol);
}


float IBC::get_expdata(byte command)           // get values for parameters which has 11 bits for mantissa and the initial 5 bits used as the exponent for 2 (iout, temperature, vin, power etc.)
{
  byte x, y;
  get_data(command, 2);                             // obtain data and retreive MSB and LSB
  x = _messagebuff[0];
  y = _messagebuff[1];
  int mant1 = ((y % 8) * 256 + x);
  int mant2 = mant1 - 2048 * (mant1 > 1024);       //(-1024) * (mant1 >= 1024) + (mant1 % 1024);
  byte a = y / 8;                                  // obtain 5 bit data of the exponent
  int e = a - 32 * (a > 16);                       // get 2's compliment of the exponent
  float p = mant2 * float(pow(2, e));              // calculate corresponding value
  return (p);
}


float IBC::get_vout()                               // get vout of the IBC
{
  float v = get_9expdata(0x8b);
  if (!comm_flag)
  {
    last_vout = v;
    return v;
  }
  else
  {
    return 0xff;                                    // return 0xff if any discrepancy
  }
}

float IBC::get_vout_command()
{
  float v = get_9expdata(0x21);
  if (!comm_flag)
    return v;
  else
  {
    return 0xff;
  }
}

float IBC::get_vtrim()                              // get vtrim value set for the IBC
{
  float v = get_9expdata(0x22);
  if (!comm_flag)
    return v;
  else
  {
    return 0xff;
  }
}

float IBC::get_mfr_vout_min()                             // get vout of the IBC
{
  float v = get_9expdata(0xa4);
  if (!comm_flag)
    return v;
  else
  {
    return 0xff;                                    // return 0xff if any discrepancy
  }
}

float IBC::get_mfr_vout_max()                             // get vout of the IBC
{
  float v = get_9expdata(0xa5);
  if (!comm_flag)
    return v;
  else
  {
    return 0xff;                                    // return 0xff if any discrepancy
  }
}

float IBC::get_vin()                                    // get input voltage value for the IBC
{
  float v = get_expdata(0x88);
  if (!comm_flag)
    return v;
  else
  {
    return 0xff;
  }
}


float IBC::get_iin()                                         // get input current (not sure of proper function)
{
  float v = get_expdata(0xA2);
  if (!comm_flag)
    return v;
  else
  {
    return 0xff;
  }
}


float IBC::get_iout()                                         // get output current
{
  float v = get_expdata(0x8c);
  if (!comm_flag)
    return v;
  else
  {
    return 0xff;
  }
}


float IBC::get_temp()                                         // get temperature of the IBC
{
  float v = get_expdata(0x8d);
  if (!comm_flag)
    return v;
  else
  {
    return 0xff;
  }
}


float IBC::get_pout()                                           // get output power
{
  float v = get_expdata(0x96);
  if (!comm_flag)
    return v;
  else
  {
    return 0xff;
  }
}


float IBC::get_pin()                                            // get input power
{
  float v = get_expdata(0xA3);
  if (!comm_flag)
    return v;
  else
  {
    return 0xff;
  }
}


void IBC::send_command(byte command)    // Function to transmit a single command byte (Clear fault, store_data etc.)
{
  int count = 0;
  if (!comm_flag)
    if (i2c_start(_port * 2 + I2C_WRITE) == 0)
    {
      _CRCbuff[count++] = _port * 2;
      i2c_write(command);                  // transmit command
      _CRCbuff[count++] = command;
      _pecbyte2 = getPECbyte(count);        // calculate PEC byte
      i2c_write(_pecbyte2);                // transmit PEC byte
      comm_flag = i2c_stop();               // end transmission
    }
    else
    {
      comm_flag = 1;
    }
}


void IBC::set_byte(byte command, byte data)   // Function to set a byte in registers (Operation, configuration etc.)
{
  int count = 0;
  if (!comm_flag)
    if (i2c_start(_port * 2 + I2C_WRITE) == 0)
    {
      _CRCbuff[count++] = _port * 2;
      i2c_write(command);                  // transmit register address
      _CRCbuff[count++] = command;
      i2c_write(data);                     // transmit set value byte for the register
      _CRCbuff[count++] = data;
      _pecbyte2 = getPECbyte(count);        // Calculate PEC
      i2c_write(_pecbyte2);                // transmit PEC
      comm_flag = i2c_stop();               // end transmission
    }
    else
    {
      comm_flag = 1;
    }
}


void IBC::set_word(byte command, byte lowbyte, byte highbyte) // Function to set word in registers (Vout_command, Vout_trim etc.)
{
  int count = 0;
  if (!comm_flag)
    if (i2c_start(_port * 2 + I2C_WRITE) == 0)
    {
      _CRCbuff[count++] = _port * 2;
      i2c_write(command);                  // transmit corresponding registry address
      _CRCbuff[count++] = command;
      i2c_write(lowbyte);                  // transmit LSB
      _CRCbuff[count++] = lowbyte;
      i2c_write(highbyte);                 // transmit MSB
      _CRCbuff[count++] = highbyte;
      _pecbyte2 = getPECbyte(count);        // calculate PEC
      i2c_write(_pecbyte2);                // transmit PEC
      comm_flag = i2c_stop();               // end transmission
    }
    else
    {
      comm_flag = 1;
    }
}


void IBC::get_setcommand(float num, int n)          // get set command bytes (high byte and low byte) for values using 5 bit power and 11 bit mantissa
{ // num is value and n = power
  int dnum = num * pow(2, -n);
  byte power;
  if (dnum <= 1024)
    lowbyte = dnum % 256;
  if (n < 0)
    power = 32 - n - 1;
  else
    power = n;
  highbyte = dnum / 256 + power * 8;
}


void IBC::get_setvoltcommand(float num)             // get set command bytes (high byte and low byte) for voltage values (16 bit mantissa and -9 for power)
{
  int dnum1 = num * pow(2, 9);
  long dnum2;
  if (dnum1 < 0)
    dnum2 = pow(2, 16) + dnum1;
  else
    dnum2 = dnum1;
  highbyte = dnum2 / 256;
  lowbyte = dnum2 % 256;
}


void IBC::set_vout(float vout)                     // set vout value
{
  get_setvoltcommand(vout);                         // calculate set bytes for the value
  set_word(0x21, lowbyte, highbyte);                // execute set word function
}


void IBC::set_vtrim(float vtrim)                  // set vtrim value
{
  get_setvoltcommand(vtrim);
  set_word(0x22, lowbyte, highbyte);
}


void IBC::clear_fault()                           // clear faults
{
  send_command(0x03);                             // send the clear fault command to IBC
}


void IBC::store_user()                            // Store the current parameter in the RAM
{
  send_command(0x15);
  delay(250);
}


void IBC::restore_default()                       // Restore the original default values of IBC
{
  send_command(0x12);
  delay(20);
}


void IBC::turnoff()                               // turn off the particular IBC
{
  set_byte(0x01, 0);
}


void IBC::turnon()                                // turn on the particular IBC
{
  set_byte(0x01, 0x80);
}


void IBC::printMessagebuff(int len)               // print the Message buffer in serial monitor (used during debug)
{
  Serial.println("Message buffer to be printed");
  for (int i = 0; i < len; i++)
  {
    Serial.print(String(String(_messagebuff[i], HEX) + " "));
  }
  Serial.println();
}


void IBC::printCRCbuff(int len)                   // print the CRC calculation buffer in serial monitor (used during debug)
{
  Serial.println("CRC buffer to be printed");
  for (int i = 0; i < len; i++)
  {
    Serial.print(String(String(_CRCbuff[i], HEX) + " "));
  }
  Serial.println();
}
/*
  Class PWMDevice
*/
PWMDevice::PWMDevice()
{
}

void PWMDevice::init(int _pin, int _val, int _mode, int _llim, int _ulim)
{
  pin               =  _pin;
  signal            =  _val;
  servo_mode        = _mode;
  pulse_lower_lim   = _llim;
  pulse_upper_lim   = _ulim;

  servo_device.attach(pin);

  setValue(signal);
}

void PWMDevice::stop()
{
  signal  = 1100;
  setValue(signal);
}

void PWMDevice::setValue(int _val)
{
  if (_val > pulse_upper_lim)
  {
    _val = pulse_upper_lim;
  }
  else if (_val < pulse_lower_lim)
  {
    _val = pulse_lower_lim;
  }

  signal  =  _val;

  if (servo_mode == SERVO_MODE_ANGLE)
  {
    servo_device.write(signal);
  }
  else if (servo_mode == SERVO_MODE_MICROSECONDS)
  {
    servo_device.writeMicroseconds(signal);
  }
}

int PWMDevice::getValue()
{
  return signal;
}

/*
  class manipulator
*/
manipulator::manipulator()
{
}

void manipulator::init(int _dir_pin, int _sig_pin, int _timer)
{
  timer          = _timer;
  direction_pin  =  _dir_pin;
  signal_pin     =  _sig_pin;
  pinMode(direction_pin, OUTPUT);
  pinMode(signal_pin, OUTPUT);
}

void manipulator::stop()
{
  analogWrite(signal_pin, 0);
}

void manipulator::run(int opt_val)
{
  analogWrite(signal_pin, opt_val);
}

void manipulator::setForward()
{
  digitalWrite(direction_pin, HIGH);
}

void manipulator::setReverse()
{
  digitalWrite(direction_pin, LOW);
}

void manipulator::setSpeed(int _mode)
{
  //  Setting Divisor Frequency
  //  0x01    1       31372.55
  //  0x02    8       3921.16
  //  0x03    64      490.20   (Default)
  //  0x04    256     122.55
  //  0x05    1024    30.64

  if (_mode >= 1 && _mode <= 7)
  {
    speed  =  _mode;
    timer =  (timer & 0b11111000) | speed;
  }
}

int manipulator::getSpeed()
{
  return speed;
}

/*
  class analogSensor
*/

analogSensor::analogSensor()
{
}

analogSensor::analogSensor(int _pin, int type)
{
  init(_pin, type);
}

void analogSensor::init(int _pin, int type)
{
  pin   = _pin;
  mode  = type;
  value = 0;

  if (type == OUTPUT) //PWM
  {
    pinMode(pin, type);
    analogWrite(pin, value);
  }
}

int analogSensor::getValue()
{
  if (mode == OUTPUT)
  {
    return value;
  }
  else if (mode == INPUT)
  {
    return readPin();
  }
}

void analogSensor::setValue(int val)
{
  if (mode == OUTPUT)
  {
    value = val;
    analogWrite(pin, value);
  }
}

int analogSensor::readPin()
{
  if (mode == INPUT)
  {
    value = analogRead(pin);
    return value;
  }
  else return value;
}

int analogSensor::readAverage(int numSamples)
{
  if (mode == INPUT)
  {
    unsigned long sum = 0;
    int _val = 0, _max = 0, _min = 1024;

    for (int i = 0; i < numSamples; i++)
    {
      _val = readPin();
      sum  += _val;
      if (_val >= _max) _max = _val;
      if (_val <= _min) _min = _val;
    }

    value = (int)((sum - _max - _min) / (numSamples - 2));
    return value;
  }
  else return value;
}


/*
  Class digitalSensor
*/

digitalSensor::digitalSensor()
{
}

digitalSensor::digitalSensor(int _pin, int type)
{
  init(_pin, type);
}

void digitalSensor::init(int _pin, int type)
{
  pin  = _pin;
  mode = type;
  pinMode(pin, mode);

  if (mode == OUTPUT)
  {
    setLow();
  }
}

void digitalSensor::setHigh()
{
  if (mode == OUTPUT)
  {
    digitalWrite(pin, HIGH);
    high = true;
    low  = false;
  }
}

void digitalSensor::setLow()
{
  if (mode == OUTPUT)
  {
    digitalWrite(pin, LOW);
    high = false;
    low  = true;
  }
}

void digitalSensor::toggle()
{
  if (mode == OUTPUT)
  {
    if (isHigh()) setLow();
    else if (isLow()) setHigh();
  }
}

bool digitalSensor::isHigh()
{
  if (mode == OUTPUT)
  {
    return high;
  }
  else if (mode == INPUT)
  {
    return readPin() == HIGH ? true : false;
  }
}

bool digitalSensor::isLow()
{
  if (mode == OUTPUT)
  {
    return low;
  }
  else if (mode == INPUT)
  {
    return readPin() == LOW ? true : false;
  }
}

int digitalSensor::readPin()
{
  if (mode == INPUT)
  {
    return digitalRead(pin);
  }
  else if (mode == OUTPUT)
  {
    if (isHigh()) return HIGH;
    if (isLow())  return LOW;
  }
}
#endif
