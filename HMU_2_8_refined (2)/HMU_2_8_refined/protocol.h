#ifndef _PROTOCOL_H
#define _PROTOCOL_H

#include "QueueArray.h"

// Protocol Variables
#define PACKET_SIZE             6
#define MAX_QUEUE_SIZE          50
#define NULL_VAL                0x00
#define DEBUG_SERIAL            CPU_SERIAL
#define DEBUG_IDENTIFIER        '*'
#define DEBUG_ALL               1
#define DEBUG_NONE              2

// CPU Connection Valiables
#define CPU_ALIVE_TIMEOUT       500
unsigned long last_instruction_timestamp = 0;
int debug_level = DEBUG_NONE;

// CPU to HMU Speed Test Variables
uint16_t _packet_counter = 0, _sample_size;
unsigned long t0, tn;

// IBC Events

#define EVT_TURN_OFF               48
#define EVT_TURN_ON                49
#define EVT_CLEAR_FAULTS           50
#define EVT_SET_VOUT               51
#define EVT_SET_VTRIM              52
#define EVT_RESTORE_DEFAULT        53
#define EVT_SAVE_CONFIGURATION     54
#define EVT_IBC_STRING             60
#define EVT_REQUEST_DATA_STRING    61
#define EVT_SET_DEBUG_LEVEL        252
#define EVT_SPEED_TEST             253

// Main Board Data String Declarations
#define IBC_DATA_STRING_IDENTIFIER     "$IBC"
#define HMU_DATA_STRING_IDENTIFIER     "$HMU"
#define HMU_DATA_STRING_PARAM_COUNT    5
int hmu_data_string[HMU_DATA_STRING_PARAM_COUNT];

class Instruction
{
  private:

  public:
    byte identifier;
    byte event;
    byte value[2];
    byte checksum[2];

    Instruction();
};

// Function Prototypes
void checkForSerialReceive();
void parseInstructions();
void trimQueue(QueueArray <unsigned char> *, unsigned char);
void getInstructions();
void processInstructionsQueue();
void executeInstruction(Instruction);

#endif
