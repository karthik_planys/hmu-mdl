#ifndef _UTILITY_H
#define _UTILITY_H

void clearI2CBuffer ();
bool CRCCheck (unsigned char *, int);
//void stopAllDevices ();
void i2c_scanner();
void IBC_initialize();
int sendIBCDataString();
void status_led_blink (int, int);
void sendDebugData (String, bool = true);
int get_portID(unsigned int);
float get_volt(unsigned int);
void createHMUDataString ();
void sendHMUDataString ();
float calculateTemperature(float);
#endif
