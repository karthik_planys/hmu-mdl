#ifndef _UTILITY_INO
#define _UTILITY_INO

void clearI2CBuffer ()
{
  while (Wire.available())
  {
    Wire.read();
  }
}

bool CRCCheck (unsigned char *data, int len)
{
  unsigned short checksum = CRC16.xmodem(data, len);
  
  if (checksum == 0x0)
  {
    return true;
  }
  else
  {
    return false;
  }
}

void i2c_scanner() // Optional code for scanning for i2c addresses.
{
  int count = 0;
  Serial.print("I2C address Detected=");
  for (int i = 1; i < 255; i++)
  {
    int comm_flag = 1;
    comm_flag = i2c_start(i * 2 + I2C_WRITE); // Initialize a send request on a particular address
    if (!comm_flag)     // For valid addresses an acknowledgement is generated and communication is error-free
    {
      port[count++] = i;     // update the address directory
      Serial.print(String(i) + " ");  // display the address
    }
    else
      i2c_stop();   // If device is not present then close the request for generating new requests
    if (count == portcount) // under normal conditions the number of devices detected = number of devices placed on the bus
    {
      Serial.println();
      break;        // stop searching when all the devices are detected
    }
  }
  Serial.println();
}


void IBC_initialize() // initialization code for starting of IBCs and finding non-responding IBCs with i2c communication problems
{
  byte prob_IBC[] = {0, 0, 0, 0};       // buffer array for IBCs showing i2c communication issues
  int prob_count = 0;                   // counting the number of problematic IBCs
  int esum = 0;                         // temperary variable for counting
  for (int i = 0; i < portcount; i++)
  {
    IBC_Array[i].turnoff();             // Turn off the IBC
    if (IBC_Array[i].comm_flag)         // Check for errors in I2C communication
    {
      CPU_SERIAL.println("I2C Problem at " + String(i + 1));
      prob_IBC[prob_count++] = i + 1;   // update the problematic IBC address in the data
    }
    else
    {
      CPU_SERIAL.println("IBC-" + String(i + 1) + "-Turned OFF"); // update user that the IBC has been turned off
      esum++;
      IBC_Array[i].set_vout(operating_voltage);                // set the operating voltage
      IBC_Array[i].set_vtrim(0);                               // Nullify the value of trim voltage if any
      IBC_Array[i].clear_fault();                              // Clear all faults and errors
    }
  }
  if (esum == portcount)                                    // if number of proper IBCs  = no. of connected IBCs (All clear in the communication bus)
  {
    CPU_SERIAL.println("All OK. Turning ON IBCs for operations");                           // Indicate ALL OK condition to the user
    for (int i = 0; i < portcount; i++) //Turn on IBCs for Operation
      IBC_Array[i].turnon();                                      // Turn on all the IBCs for the normal operation

  }
}
void status_led_blink (int count, int  duration)
{
  int dt = (duration / count) / 2;
  while (count > 0)
  {
    status_led.setHigh();
    delay(dt);
    status_led.setLow();
    delay(dt);

    count--;
  }
}

/*void sendDebugData (String str, bool newline)
{
  if (debug_level == DEBUG_ALL)
  {
    DEBUG_SERIAL.print (DEBUG_IDENTIFIER);
    DEBUG_SERIAL.print (str);

    if (newline) DEBUG_SERIAL.print ("\r\n");
  }
}*/

int sendIBCDataString()
{
  String str;
  unsigned int sword;
  bool comm_check = 0;
  int sumflag = 0;

  createHMUDataString();

  CPU_SERIAL.print(IBC_DATA_STRING_IDENTIFIER);

  for (int i = 0; i < portcount; i++)
  {
    CPU_SERIAL.print(",IBC-");
    CPU_SERIAL.print(i + 1);
    CPU_SERIAL.print("@");
    CPU_SERIAL.print(port[i]);
    unsigned int sword = IBC_Array[i].get_status_word();
    if (!IBC_Array[i].comm_flag)
    {
      float vout = IBC_Array[i].get_vout();
      CPU_SERIAL.print(":vout->");
      CPU_SERIAL.print(vout);
    }
    else
      sumflag++;

    if (!IBC_Array[i].comm_flag)
    {
      float pout = IBC_Array[i].get_pout();
      CPU_SERIAL.print(";Pout->");
      CPU_SERIAL.print(pout);
    }
    else
      sumflag++;

    if (!IBC_Array[i].comm_flag)
    {
      float vin = IBC_Array[i].get_vin();
      CPU_SERIAL.print(";Vin->");
      CPU_SERIAL.print(vin);
    }
    else
      sumflag++;

    if (!IBC_Array[i].comm_flag)
    {
      float iout = IBC_Array[i].get_iout();
      CPU_SERIAL.print(";Iout->");
      CPU_SERIAL.print(iout);
    }
    else
      sumflag++;

    if (!IBC_Array[i].comm_flag)
    {
      float temperature = IBC_Array[i].get_temp();
      CPU_SERIAL.print(";Temperature->");
      CPU_SERIAL.print(temperature);
    }
    else
      sumflag++;

    if (!IBC_Array[i].comm_flag)
    {
      String str = IBC_Array[i].get_status_data();
      CPU_SERIAL.print(";Status->");
      CPU_SERIAL.print(str);
    }
    else
      sumflag++;

    if (sumflag > 0)
    {
      CPU_SERIAL.print(":I2C Error");
      comm_check += sumflag;
      sumflag = 0;
    }
    CPU_SERIAL.print("#");
  }
  sendHMUDataString();
  return (comm_check);
}

int get_portID(unsigned int data)
{
  return (data / 16384);
}

float get_volt(unsigned int data)
{
  int  val = data % 16384;
  float volt = float(((val - 16384) * (val > 8192) + (val) * (val < 8192))) / 512.0;
  return (volt);
}

void createHMUDataString ()
{
  uint16_t _value = 0;
  int counter     = 0;
  int idx = 0;

  // Update Sensors
  for (counter = 0; counter < 5; counter++) {
    _value = _value + Hull_Leak_Sensor.readPin();
  }

  HULL_LEAK_STATUS = _value/5;
  _value                = 0;
  for (counter = 0; counter < 5; counter++) {
    _value = _value + Cam_Enc_Leak_Sensor.readPin();
  }
  CAM_ENC_LEAK_STATUS   = _value / 5;
  transmitbyte = (PG2.readPin()) * pow(2, 0) + (PG3.readPin()) * pow(2, 1) + (PG4.readPin()) * pow(2, 2) + (PG.readPin()) * pow(2, 3) + (dc_ok_acdc.readPin()) * pow(2, 4) + (t_alarm_acdc.readPin()) * (pow(2, 5));

  Ambient_Temperature   = calculateTemperature(analogRead(Ambient_Temp_Sense_Pin));
  temperature1                 = calculateTemperature(analogRead(NTC_1));
  temperature2                 = calculateTemperature(analogRead(NTC_2));

  hmu_data_string[idx++] = HULL_LEAK_STATUS;
  hmu_data_string[idx++] = CAM_ENC_LEAK_STATUS;
  hmu_data_string[idx++] = transmitbyte;
  hmu_data_string[idx++] = Ambient_Temperature;
  hmu_data_string[idx++] = temperature1;
  hmu_data_string[idx++] = temperature2;
}

void sendHMUDataString()
{
  int idx;
  CPU_SERIAL.print (HMU_DATA_STRING_IDENTIFIER);
  CPU_SERIAL.print (",");
  for (idx = 0; idx < HMU_DATA_STRING_PARAM_COUNT; idx++)
  {
    CPU_SERIAL.print (hmu_data_string[idx]);
    CPU_SERIAL.print (",");
  }
  CPU_SERIAL.print (hmu_data_string[idx]);
  CPU_SERIAL.print ("\r\n");
}

float calculateTemperature(float voltage) {
  float resistance  = SERIESRESISTOR * (voltage / (1023 - voltage));    // Calculating NTC resistance
  float temperature;
  temperature         = resistance / THERMISTORNOMINAL;                   // (R/Ro)
  temperature         = log(temperature);                                 // ln(R/Ro)
  temperature         /= BCOEFFICIENT;                                    // 1/B * ln(R/Ro)
  temperature         += 1.0 / (TEMPERATURENOMINAL + 273.15);             // + (1/To)
  temperature         = 1.0 / temperature;                                // Invert
  temperature         -= 273.15;                                          // convert to C
  return temperature;
}
#endif
